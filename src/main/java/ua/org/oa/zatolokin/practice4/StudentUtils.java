package ua.org.oa.zatolokin.practice4;

import java.util.*;

/**
 * Created by user on 25.04.2016.
 */
public class StudentUtils {

    public static List<Student> students = new ArrayList<>();

    public static Map<String, Student> createMapFromList(List<Student> st) {
        Map<String, Student> map = new HashMap<>();
        for (int i = 0; i < st.size(); i++) {
            map.put(st.get(i).getFirstName().concat(" ").concat(st.get(i).getLastName()), st.get(i));
        }
        return map;
    }

    public static void printStudents(List<Student> st, int course) {
        ListIterator<Student> studentIterator = st.listIterator();
        while (studentIterator.hasNext()) {
            if (studentIterator.next().getCourse() == course) {
                studentIterator.previous();
                System.out.println(studentIterator.next().toString());
            }
        }
    }

    public static List<Student> sortStudent(List<Student> st) {
        st.sort((a, b) -> {
            int i = a.getFirstName().compareTo(b.getFirstName());
            if (i == 0)
                i = a.getLastName().compareTo(b.getLastName());
            return i;
        });
        return st;
    }
}
