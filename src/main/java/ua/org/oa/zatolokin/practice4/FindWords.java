package ua.org.oa.zatolokin.practice4;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 26.04.2016.
 */
public class FindWords {

    public static final String FILE_NAME = "C:/Users/user/Downloads/Romeo.txt";

    public static String fileReader(String fileName) {
        File file = new File(fileName);
        StringBuilder contents = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "windows-1251"));
            String text = null;


            while ((text = reader.readLine()) != null) {
                contents.append(text)
                        .append(System.getProperty(
                                "line.separator"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return contents.toString();
    }


    public static Map<String, Integer> changeToMap() { // task 5
        Map<String, Integer> words = new HashMap<>();
        String regex = "\\w+";
        Pattern pattern = null;
        String str = null;
        pattern = pattern.compile(regex);
        Matcher matcher = pattern.matcher(fileReader(FILE_NAME));
        while (matcher.find()) {
            str = matcher.group();
            int count = 1;
            if (words.containsKey(str)) {
                count = words.get(str);
                words.put(str, ++count);
            } else words.put(str, count);
        }
        return words;
    }

    public static enum SortingMethod {
        KEY_UP, KEY_DOWN, VALUE_UP, VALUE_DOWN;
    }

    public static Map<String, Integer> changeToMap(SortingMethod sortingMethod) { // task 6
        Map<String, Integer> words = null;
        String regex = "\\w+";
        Pattern pattern = null;
        String str = null;
        pattern = pattern.compile(regex);
        Matcher matcher = pattern.matcher(fileReader(FILE_NAME));
        switch (sortingMethod) {
            case KEY_UP: {
                words = new TreeMap<>((a, b) -> a.compareTo(b));
                while (matcher.find()) {
                    str = matcher.group();
                    int count = 1;
                    if (words.containsKey(str)) {
                        count = words.get(str);
                        words.put(str, ++count);
                    } else words.put(str, count);
                }
            } break;
            case KEY_DOWN: {
                words = new TreeMap<>((a, b) -> b.compareTo(a));
                while (matcher.find()) {
                    str = matcher.group();
                    int count = 1;
                    if (words.containsKey(str)) {
                        count = words.get(str);
                        words.put(str, ++count);
                    } else words.put(str, count);
                }
            } break;
            case VALUE_UP: {
                Map<String, Integer> map = new HashMap<>();
                while (matcher.find()) {
                    str = matcher.group();
                    int count = 1;
                    if (map.containsKey(str)) {
                        count = map.get(str);
                        map.put(str, ++count);
                    } else map.put(str, count);
                }
                List<Map.Entry<String, Integer>> list = new ArrayList(map.entrySet()); // write to new list to sorting
                Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
                    @Override
                    public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                        return a.getValue() - b.getValue();
                    }
                });
                words = new LinkedHashMap<>();
                for (int i = 0; i < list.size(); i++) {
                    words.put(list.get(i).getKey(), list.get(i).getValue()); // write sorted list to LinkedHashMap
                }
            } break;
            case VALUE_DOWN: {
                Map<String, Integer> map = new HashMap<>();
                while (matcher.find()) {
                    str = matcher.group();
                    int count = 1;
                    if (map.containsKey(str)) {
                        count = map.get(str);
                        map.put(str, ++count);
                    } else map.put(str, count);
                }
                List<Map.Entry<String, Integer>> list = new ArrayList(map.entrySet());
                Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
                    @Override
                    public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                        return b.getValue() - a.getValue();
                    }
                });
                words = new LinkedHashMap<>();
                for (int i = 0; i < list.size(); i++) {
                    words.put(list.get(i).getKey(), list.get(i).getValue());
                }
            } break;
        }
        return words;
    }
}
