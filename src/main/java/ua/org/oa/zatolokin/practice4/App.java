package ua.org.oa.zatolokin.practice4;

import java.util.Map;

/**
 * Created by user on 26.04.2016.
 */
public class App {
    public static void main(String[] args) {
        StudentUtils.students.add(new Student("Petya", "Vasylyev", 4));
        StudentUtils.students.add(new Student("Vasya", "Petrov", 1));
        StudentUtils.students.add(new Student("Masha", "Klimenko", 4));
        StudentUtils.students.add(new Student("Petya", "Medvedev", 2));
        StudentUtils.students.add(new Student("Katya", "Pushkina", 5));
        Map<String, Student> map = StudentUtils.createMapFromList(StudentUtils.students);
        System.out.println(map.toString());
        StudentUtils.printStudents(StudentUtils.students, 4);
        System.out.println(StudentUtils.sortStudent(StudentUtils.students));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(FindWords.changeToMap().toString());
        System.out.println(FindWords.changeToMap(FindWords.SortingMethod.KEY_UP));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(FindWords.changeToMap(FindWords.SortingMethod.VALUE_UP));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(FindWords.changeToMap(FindWords.SortingMethod.VALUE_DOWN));
    }
}
