package ua.org.oa.zatolokin.practice2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 15.04.2016.
 */
public class Magazine {

    public List<Computer> arrayOfComputer = new ArrayList<>();

    public void addToArray() {

        for (int i = 0; i < Parser.toNameArray().size(); i++) {
            arrayOfComputer.add(new Computer(Parser.toNameArray().get(i), Parser.toCostArray().get(i), Parser.toDescriptionArray().get(i)));
        }
    }

    public Computer getComputer(String st) {
        for (int i = 0; i < Parser.toNameArray().size(); i++) {
            if (st.equalsIgnoreCase(arrayOfComputer.get(i).getName()))
                return arrayOfComputer.get(i);
        }
        System.out.println("Computer with this name is not!");
        return null;
    }

    @Override
    public String toString() {
        return "Magazine{" + arrayOfComputer + '}';
    }

    public Magazine() {
        addToArray();
    }

}
