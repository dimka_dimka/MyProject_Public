package ua.org.oa.zatolokin.practice2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 11.04.2016.
 */
public class Parser {

    public static final String FILE_NAME = "C:/Users/user/Downloads/source.html";

    public static String fileReader(String fileName) {
        File file = new File(fileName);
        StringBuilder contents = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "windows-1251"));
            String text = null;


            while ((text = reader.readLine()) != null) {
                contents.append(text)
                        .append(System.getProperty(
                                "line.separator"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return contents.toString();
    }

    public static List<String> parse(String st, String regex) { // parse and write to list
        Pattern pattern = null;
        List<String> str = new ArrayList<>();
        pattern = pattern.compile(regex);
        Matcher matcher = pattern.matcher(st);
        while (matcher.find()) {
            str.add(matcher.group());
        }

        return str;
    }

    public static String parseStr(String st, String regex) { // parse one string
        Pattern pattern = null;
        String str = null;
        pattern = pattern.compile(regex);
        Matcher matcher = pattern.matcher(st);
        if (matcher.find()) {
            str = matcher.group();
        }

        return str;
    }

    public static List<Integer> toCostArray() { // create array of notebook's prices
        String regex = ">\\d{4,5}\\s";
        String regex2 = "\\d{4,5}";
        List<String> cost;
        cost = parse(fileReader(FILE_NAME), regex);
        List<Integer> costInt = new ArrayList<>();
        for (int i = 0; i < cost.size(); i++) {
            costInt.add(Integer.parseInt(parseStr(cost.get(i), regex2)));
        }
        return costInt;
    }

    public static List<String> toNameArray() { // create array of notebook's names
        String regex = "item_name.+?>"; // find wide expressions
        String regex2 ="�������.+>"; // narrow our expressions
        List<String> name; // for regex
        List<String> name2 = new ArrayList<>(); // for regex2
        List<String> name3 = new ArrayList<>(); // final names (without last symbols)
        name = parse(fileReader(FILE_NAME), regex);
        for (int i= 0;i<name.size();i++){
            name2.add(parseStr(name.get(i),regex2));
            name3.add(name2.get(i).substring(0,name2.get(i).length()-2)); // copy without two last symbols
        }
        return name3;
    }

    public static List<String> toDescriptionArray(){ // create aray of notebook's descriptions
        String regex = "description.+<br";
        String regex2 ="1.+<";
        List<String> descript;
        List<String> descript2 = new ArrayList<>();
        List<String> descript3 = new ArrayList<>();
        descript = parse(fileReader(FILE_NAME), regex);
        descript.add(6,"description\">15,6\" / 1366x768 / ����������� ������ - ��������� / ��������� - AMD Quad-Core A4-5000, 1,5 ��� / AMD A70M / 4 �� / ����� HDD - 1 �� / ATI Mobility Radeon HD8570M, 1 �� / DVD-RW / 100 ����/� / 802.11 b/g/n / Bluetooth / HDMI / USB 2.0 - 1 ��. / USB 3.0 - 2 ��. / ����-����� / Web-������ - 1,3 �� / ������� - 6 ����� / �� - DOS / ������ / 380�260�34 �� / 2,6 ��<br");
        for (int i= 0;i<descript.size();i++){
            descript2.add(parseStr(descript.get(i),regex2));
            descript3.add(descript2.get(i).substring(0,descript2.get(i).length()-3));
        }
        return descript3;
    }

    private Parser(){}
}

