package ua.org.oa.zatolokin.practice2;

/**
 * Created by user on 11.04.2016.
 */
public class Computer {

    private String name;
    private int cost;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", description='" + description + '\'' +
                '}';
    }

    public Computer(String name, int cost, String description) {
        this.name = name;
        this.cost = cost;
        this.description = description;
    }
}
