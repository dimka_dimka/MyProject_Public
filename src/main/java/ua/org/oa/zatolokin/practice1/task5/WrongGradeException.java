package ua.org.oa.zatolokin.practice1.task5;

/**
 * Created by user on 08.04.2016.
 */
public class WrongGradeException extends Exception {
    public WrongGradeException(String message) {
        super(message);
    }
}
