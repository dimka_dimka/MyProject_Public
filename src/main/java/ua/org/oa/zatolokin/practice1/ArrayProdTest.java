package ua.org.oa.zatolokin.practice1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by user on 04.04.2016.
 */
public class ArrayProdTest {

    @Test
    public void proTest1(){

        int[] arr = {2,3,4};
        int expect = 24;
        int pro = ArrayProd.pro(arr);
        Assert.assertEquals(expect,pro);

    }
}
