package ua.org.oa.zatolokin.practice1.task5;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 08.04.2016.
 */
public class Student {

    private String name; // student's name
    private String surname; // student's surnime
    private Group group; // student's group
    private List<Exam> exams = new ArrayList<>(); // array for storing exams
    public final int FIRST_COURSE = 1; // constant
    public final int LAST_COURSE = 6;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Group getGroup() {
        return group;
    }


    public void setGroup(int course, String faculty) throws WrongCourseException { // throws exception if user input wrong course
        if (course >= FIRST_COURSE && course <= LAST_COURSE)
            this.group = new Group(course, faculty); // creat new object with input parameters
        else
            throw new WrongCourseException("You input wrong course! It must be from " + FIRST_COURSE + " to " + LAST_COURSE + " !!"); // throws exception if user input wrong course
    }

    public void addExam(ExamName examName, int grade, int year, int semester) throws WrongGradeException, WrongYearException, WrongSemesterException {
        exams.add(new Exam(examName, grade, year, semester)); // add new exam to our array
    }

    public void deleteExam(ExamName examName, int grade, int year, int semester) throws ExamAbsentException {
        int count = 0; //counter for found exams
        for (Exam ex : exams) {
            if (ex.getName() == examName && ex.getGrade() == grade && ex.getYear() == year && ex.getSemester() == semester) {
                exams.remove(ex); // find exam with input parameters and delete it
                count++;
                return;
            }
        }
        if (count == 0)
            throw new ExamAbsentException("This student didn't passed such examination!");
    }

    public int suchGrade(int grade) { // find exams which student passes with input grade
        int count = 0; // counter for found exams
        for (Exam ex : exams) {
            if (ex.getGrade() == grade)
                count++;
        }
        System.out.println("This student passed " + count + " examenations with evalution " + grade);
        return count;
    }

    public double averageBall(int year, int semester) { // we calculate average ball in entered period
        int ball = 0;
        int count = 0;
        for (Exam ex : exams) {
            if (ex.getYear() == year && ex.getSemester() == semester) { // find exam in intered period
                ball += ex.getGrade(); // and sum ball
                count++;
            }
        }
        if (count == 0) {
            System.out.println("In this period this student didn't passes any examenations!");
            return 0;
        } else {
            System.out.println("In this period this student has average ball " + (double) ball / count);
            return (double) ball / count;
        }
    }

    public List<Exam> getExams() {
        return exams;
    }

    public Student(String name, String surname, int course, String faculty) throws WrongCourseException {
        this.name = name;
        this.surname = surname;
        setGroup(course, faculty);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", " + group.toString() +
                '}';
    }

    public String ExamToString() {
        return getName().toString() + "{" +
                "exams=" + exams +
                '}';
    }
}
