package ua.org.oa.zatolokin.practice1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by user on 04.04.2016.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ArraySumTest.class, ArrayProdTest.class})
public class AllTest {


}
