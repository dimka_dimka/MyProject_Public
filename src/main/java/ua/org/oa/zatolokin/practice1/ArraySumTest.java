package ua.org.oa.zatolokin.practice1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by user on 04.04.2016.
 */
public class ArraySumTest {

    @Test
    public void sumTest1(){

        int[] arr = {3,4,5};
        int expect = 12;
        int sum = ArraySum.sum(arr);
        Assert.assertEquals(expect,sum);

    }

    @Test
    public void summaTest2(){
        int[] arr = {3,5,7};
        ArraySum arrSum = new ArraySum(arr);
        int expect = 15;
        int sum = arrSum.summa();
        Assert.assertEquals(expect,sum);

    }

    @Test(expected = NullPointerException.class)
    public void sumTest3() {

        int[] arr = {3, 4, 5};
        int expect = 12;
        int sum = ArraySum.sum(null);
        Assert.assertEquals(expect, sum);
    }

}
