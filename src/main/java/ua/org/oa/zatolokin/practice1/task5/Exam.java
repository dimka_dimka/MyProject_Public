package ua.org.oa.zatolokin.practice1.task5;

/**
 * Created by user on 08.04.2016.
 */
public class Exam {

    private ExamName name; // enum of names of exams
    private int grade;
    private int year;
    private int semester;
    public final int LOWEST_GRADE = 1;
    public final int HIGHEST_GRADE = 5;
    public final int BEGIN_LERNING_YEAR = 2010;
    public final int END_LERNING_YEAR = 2016;
    public final int FIRST_SEMESTER = 1;
    public final int LAST_SEMESTER = 2;

    public ExamName getName() {
        return name;
    }

    public void setName(ExamName name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) throws WrongGradeException {
        if (grade >= LOWEST_GRADE && grade <= HIGHEST_GRADE)
            this.grade = grade;
        else throw new WrongGradeException("You input wrong grade! It must be from "+LOWEST_GRADE+" to "+HIGHEST_GRADE+" !!");
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) throws WrongYearException{
        if (year>=BEGIN_LERNING_YEAR && year<=END_LERNING_YEAR)
        this.year = year;
        else throw new WrongYearException("You input wrong year! It must be from "+BEGIN_LERNING_YEAR+" to "+END_LERNING_YEAR+" !!");
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) throws WrongSemesterException{
        if (semester==FIRST_SEMESTER || semester==LAST_SEMESTER)
        this.semester = semester;
        else throw new WrongSemesterException("You input wrong semester! It must be only "+FIRST_SEMESTER+" or "+LAST_SEMESTER+" !!");
    }

    @Override
    public String toString() {
        return "Exam{" +
                "name=" + name +
                ", grade=" + grade +
                ", year=" + year +
                ", semester=" + semester +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Exam exam = (Exam) o;

        if (grade != exam.grade) return false;
        if (year != exam.year) return false;
        if (semester != exam.semester) return false;
        return name == exam.name;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + grade;
        result = 31 * result + year;
        result = 31 * result + semester;
        return result;
    }

    public Exam(ExamName name, int grade, int year, int semester) throws WrongGradeException, WrongYearException, WrongSemesterException {
        setName(name);
        setGrade(grade);
        setYear(year);
        setSemester(semester);
    }
}
