package ua.org.oa.zatolokin.practice1.task5;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by user on 09.04.2016.
 */
public class StudentTest {

    @Test
    public void setGroupTest() throws WrongCourseException {
        int expectCourse = 4;
        String expectNameGroup = "RT";
        Student st = new Student("Petya", "Fedorov", expectCourse, expectNameGroup);
        Assert.assertEquals(expectCourse, st.getGroup().getCourse());
        Assert.assertEquals(expectNameGroup, st.getGroup().getFaculty());

    }

    @Test(expected = WrongCourseException.class)
    public void setGroupExceptionTest() throws WrongCourseException {
        int wrongCourse = 7;
        Student st = new Student("Petya", "Fedorov", wrongCourse, "RT");
        //  Assert.assertEquals(wrongCourse, st.getGroup().getCourse());

    }

    @Test
    public void addExamTest() throws Exception {
        Student st = new Student("Petya", "Fedorov", 5, "RT");
        ExamName expectName = ExamName.PHILOSOPHY;
        int exceptGrade = 4;
        int expectYear = 2015;
        int excpectSemester = 2;
        st.addExam(expectName, exceptGrade, expectYear, excpectSemester); // add new exam and compare it atributes to except atributes
        Assert.assertEquals(expectName, st.getExams().get(st.getExams().size() - 1).getName());
        Assert.assertEquals(exceptGrade, st.getExams().get(st.getExams().size() - 1).getGrade());
        Assert.assertEquals(expectYear, st.getExams().get(st.getExams().size() - 1).getYear());
        Assert.assertEquals(excpectSemester, st.getExams().get(st.getExams().size() - 1).getSemester());
    }

    @Test(expected = WrongGradeException.class)
    public void addExamGradeExceptionTest() throws Exception {
        Student st = new Student("Petya", "Fedorov", 5, "RT");
        int wrongGrade = 6;
        st.addExam(ExamName.PHILOSOPHY, wrongGrade, 2016, 2);
    }

    @Test(expected = WrongYearException.class)
    public void addExamYearExceptionTest() throws Exception {
        Student st = new Student("Petya", "Fedorov", 5, "RT");
        int wrongYear = 1728;
        st.addExam(ExamName.PHILOSOPHY, 4, wrongYear, 2);
    }

    @Test(expected = WrongSemesterException.class)
    public void addExamSemesterExceptionTest() throws Exception {
        Student st = new Student("Petya", "Fedorov", 5, "RT");
        int wrongSemester = 5;
        st.addExam(ExamName.PHILOSOPHY, 4, 2014, wrongSemester);
    }

    @Test
    public void deleteExamTest() throws Exception{
        Student st = new Student("Petya", "Fedorov", 5, "RT");
        st.addExam(ExamName.ENGLISH, 5, 2014, 2);
        st.addExam(ExamName.MATHEMATIC, 4, 2014, 2);
        st.addExam(ExamName.PHILOSOPHY, 3, 2016, 1);
        int currentSize = st.getExams().size();
        st.deleteExam(ExamName.MATHEMATIC, 4, 2014, 2);
        int expectSize = st.getExams().size();
        Assert.assertEquals(expectSize,currentSize-1); // compare except size of array to actual size after remove chosen exam
    }

    @Test(expected = ExamAbsentException.class)
    public void deleteExamExceptionTest() throws Exception{
        Student st = new Student("Petya", "Fedorov", 5, "RT");
        st.deleteExam(ExamName.ENGLISH, 5, 2014, 2);
    }

    @Test
    public void suchGradeTest() throws Exception {
        Student st = new Student("Petya", "Fedorov", 5, "RT");
        st.addExam(ExamName.MATHEMATIC, 4, 2014, 2);
        st.addExam(ExamName.PHILOSOPHY, 3, 2016, 1);
        st.addExam(ExamName.PHYSICS, 3, 2014, 2);
        int chosenGrade = 3;
        int exceptedResult = 2;
        int actualResult = st.suchGrade(chosenGrade);
        Assert.assertEquals(exceptedResult, actualResult);
    }

    @Test
    public void averageGradeTest() throws Exception {
        Student st = new Student("Petya", "Fedorov", 5, "RT");
        st.addExam(ExamName.ENGLISH, 5, 2014, 2);
        st.addExam(ExamName.MATHEMATIC, 4, 2014, 2);
        st.addExam(ExamName.PHILOSOPHY, 3, 2016, 1);
        st.addExam(ExamName.PHYSICS, 3, 2014, 2);
        double exceptedAverage = 4;
        double actualAverage = st.averageBall(2014,2);
        Assert.assertEquals(exceptedAverage, actualAverage, 0.01);
    }
}