package ua.org.oa.zatolokin.practice1;

/**
 * Created by user on 04.04.2016.
 */
public class ArrayProd {

    public static int pro(int[] ar) {
        int pro = 1;
        for (int i = 0; i < ar.length; i++)
            pro*=ar[i];
        return pro;
    }
}
