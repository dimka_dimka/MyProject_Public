package ua.org.oa.zatolokin.practice6;

/**
 * Created by user on 26.05.2016.
 */
public class ThreadPrintName implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
