package ua.org.oa.zatolokin.practice6;

import java.util.*;

/**
 * Created by user on 26.05.2016.
 */
public class MyShedule {

    public static void printMessages(LinkedHashMap<String, Integer> map){
        List<Map.Entry<String,Integer>> mapList = new ArrayList(map.entrySet());
        new Thread(()->{
            for (int i = 0; i < mapList.size(); i++) {
                try {
                    Thread.sleep(mapList.get(i).getValue());
                    System.out.println(mapList.get(i).getKey());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
