package ua.org.oa.zatolokin.practice6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by user on 30.05.2016.
 */
public class CopyFiles { // task 4

    public static void findDir(String dirName1, String dirName2) throws FileNotFoundException {
        File file1 = new File(dirName1);
        if (!file1.exists())
            throw new FileNotFoundException();
        File file2 = new File(dirName2);
        if (!file2.exists()) {
            try {
                Files.copy(file1.toPath(), file2.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (file1.isDirectory()) {
            String[] files = FindFiles.showDirectoryContent(dirName1);
            for (int i = 0; i < files.length; i++) {
                String newPath = dirName1.concat("/").concat(files[i]); // make full path to file (file name)
                String newPath2 = dirName2.concat("/").concat(files[i]); // make full path to file2 (file name)
                new Thread(() -> {
                    try {
                        findDir(newPath, newPath2);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        }
    }
}
