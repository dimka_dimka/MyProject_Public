package ua.org.oa.zatolokin.practice6;

import java.io.*;

/**
 * Created by user on 27.05.2016.
 */
public class FindFiles { // task 3

    public static String[] showDirectoryContent(String path) {
        File file = new File(path);
        return file.list();
    }

    public static void findFileName(String path, String name, String logFile) {
        File file = null;
        String[] files = showDirectoryContent(path);
        for (int i = 0; i < files.length; i++) {
            String newPath = path.concat("/").concat(files[i]); // make full path to file (file name)
            file = new File(newPath);
            if (file.isDirectory()) {
                new Thread(() -> {
                    findFileName(newPath, name, logFile);
                }).start();
            }
            if (newPath.contains(name)) {
                writeToFile(newPath.concat("\n"), logFile);
            }
        }
    }

    private static void writeToFile(String name, String path) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true));) {
            bw.write(name);
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
