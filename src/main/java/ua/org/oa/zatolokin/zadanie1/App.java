package ua.org.oa.zatolokin.zadanie1;

/**
 * Created by user on 04.05.2016.
 */
public class App {

    public static void main (String[] args){
        Automobile bmw = new Automobile("BMW", 35000) {
            @Override
            public String toString() {
                return "Name: "+this.getName()+", cost: "+this.getPrice()+"$";
            }
        };
        System.out.println(bmw);
        Automobile audi = new Automobile("Audi", 29000) {
            @Override
            public String toString() {
                return "Brend - "+this.getName()+"\nPrice - "+this.getPrice()+"$" ;
            }
        };
        System.out.println(audi);
    }
}
