package ua.org.oa.zatolokin.zadanie1;

/**
 * Created by user on 18.04.2016.
 */
public class Calendar {

    public static int daysBetween(Date date1, Date date2) { // calculate quantity of days between two dates
        Date d1;
        Date d2;
        if (date1.getYear() - date2.getYear() > 0) {
            d1 = date1;
            d2 = date2;
        } else if (date1.getYear() < date2.getYear()) {
            d1 = date2;
            d2 = date1;
        } else {
            if (date1.dayOfTheYear() > date2.dayOfTheYear()) {
                return date1.dayOfTheYear() - date2.dayOfTheYear();
            } else
                return date2.dayOfTheYear() - date1.dayOfTheYear();
        }
        int daysQuantity = d1.dayOfTheYear();
        for (int i = d1.getYear()-1; i>d2.getYear(); i--){
            daysQuantity+=d1.getDaysOfYear(i);
        }
        daysQuantity+=(d2.getDaysOfYear()-d2.dayOfTheYear());
        return daysQuantity;
    }

    private Calendar(){}
}
