package ua.org.oa.zatolokin.zadanie1;

/**
 * Created by user on 17.04.2016.
 */
public class Date {

    private int day;
    private int year;
    private int month;

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public boolean isLeepYear() {
        Year y = new Year(year);
        return y.isLeepYear();
    }

    public boolean isLeepYear(int year) {
        Year y = new Year(year);
        return y.isLeepYear();
    }

    public int getDaysOfYear() {
        if (isLeepYear())
            return 366;
        else return 365;
    }

    public int getDaysOfYear(int year) {
        if (isLeepYear(year))
            return 366;
        else return 365;
    }

    public int dayOfTheYear(){
        int dayOfYear = day;
        for (int i=month-1;i>0;i--) {
            dayOfYear+=quantityOfDaysOfMonth(i);
        }
        return dayOfYear;
    }

    public int quantityOfDaysOfMonth() {
        Month m = new Month(month);
        return m.quantityOfDays();
    }

    public int quantityOfDaysOfMonth(int month) {
        Month m = new Month(month);
        return m.quantityOfDays();
    }

    public DayOfWeek getDayFromDate(){
        final int DAYS_OF_WEEK = 7;
        final Date date = new Date(1970,1,1);
        int totalDays = Calendar.daysBetween(this,date);
        int weeks = totalDays%DAYS_OF_WEEK;
        switch (weeks){
            case 0: return DayOfWeek.THURSDAY;
            case 1: return DayOfWeek.FRIDAY;
            case 2: return DayOfWeek.SATERDAY;
            case 3: return DayOfWeek.SUNDAY;
            case 4: return DayOfWeek.MONDAY;
            case 5: return DayOfWeek.TUESDAY;
            case 6: return DayOfWeek.WEDNESDAY;
        }
        return null;
    }

    public enum DayOfWeek {
        MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATERDAY(5), SUNDAY(6);
        public int numberOfDay;

        private DayOfWeek(int number) {
            this.numberOfDay = number;
        }
    }

    public class Year {

        private int year;

        /*public int getYear() {
            return year;
        }*/

        public Year(int year) {
            this.year = year;
        }

        public boolean isLeepYear() {
            if (year % 100 == 0 && year % 400 != 0) {
                return false;
            }
            if (year % 4 == 0)
                return true;
            else
                return false;
        }

    }

    public class Month {
        private int month;

        public int quantityOfDays() {
            if (month > 0 && month <= 12)
                switch (month) {
                    case 1:
                        return 31;
                    case 2: {
                        if (isLeepYear()) return 29;
                        else return 28;
                    }
                    case 3:
                        return 31;
                    case 4:
                        return 30;
                    case 5:
                        return 31;
                    case 6:
                        return 30;
                    case 7:
                        return 31;
                    case 8:
                        return 31;
                    case 9:
                        return 30;
                    case 10:
                        return 31;
                    case 11:
                        return 30;
                    case 12:
                        return 31;
                }
            return 0;
        }

        public Month(int month) {
            this.month = month;
        }
    }

    public class Day {
        int day;

        public Day(int day) {
            this.day = day;
        }
    }

    public Date(int year, int month, int day) {
        this.year = year;
        if (month > 0 && month <= 12)
            this.month = month;
        if (day > 0 && day <= 31)
            this.day = day;
    }
}
