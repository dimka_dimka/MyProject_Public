package ua.org.oa.zatolokin.zadanie1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by user on 18.04.2016.
 */
public class DateTest {
    @Test
    public void isLeepYearTest(){
        boolean expected = false;
        Date dat = new Date(1800,3,1);
        boolean current = dat.isLeepYear();
        Assert.assertEquals(current,expected);
    }

    @Test
    public void quantityOfDaysTest(){
        int expected = 31;
        Date dat = new Date(2012,12,1);
        int current = dat.quantityOfDaysOfMonth();
        Assert.assertEquals(current,expected);
    }

    @Test
    public void dayOfTheYearTest(){
        int expected = 70;
        Date dat = new Date(2011,3,11);
        int current = dat.dayOfTheYear();
        Assert.assertEquals(current,expected);
    }

    @Test
    public void daysBetweenTest(){
        int expected = 1134;
        Date dat1 = new Date(2011,3,11);
        Date dat2 = new Date(2008,2,1);
        int current = Calendar.daysBetween(dat1,dat2);
        Assert.assertEquals(current,expected);
    }

    @Test
    public void getDayFromDateTest(){
        Date.DayOfWeek expected = Date.DayOfWeek.MONDAY;
        Date dat = new Date(2016,4,18);
        Date.DayOfWeek current = dat.getDayFromDate();
        Assert.assertEquals(current,expected);
    }
}
