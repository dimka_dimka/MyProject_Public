package ua.org.oa.zatolokin.zadanie1;

/**
 * Created by user on 04.05.2016.
 */
public class Automobile {

    private String name;
    private int price;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Automobile{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    public Automobile(String name, int price) {
        this.name = name;
        this.price = price;
    }
}
