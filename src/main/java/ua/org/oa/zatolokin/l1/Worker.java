package ua.org.oa.zatolokin.l1;

/**
 * Created by user on 03.04.2016.
 */
public class Worker {

    private String name;
    private int solary;

    public String getName() {
        return name;
    }

    public int getSolary() {
        return solary;
    }

    public Worker(String name, int solary) {
        this.name = name;
        this.solary = solary;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", solary=" + solary +
                '}';
    }
}
