package ua.org.oa.zatolokin.task5;

import java.io.*;
import java.util.HashMap;

/**
 * Created by user on 14.06.2016.
 */
public class Translator {

    public static HashMap<String, String> writeToMapFromFile(String path) throws FileNotFoundException {
        HashMap<String, String> mapWithWords = new HashMap<>();
        File file = new File(path);
        if (!file.exists())
            throw new FileNotFoundException();
        String str = null;
        String[] st = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            while ((str = br.readLine()) != null) {
                st = str.split("[;-;]");

                mapWithWords.put(st[0], st[2]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mapWithWords;
    }

    private static StringBuilder readFromFileToStringBuilder(String path) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        File file = new File(path);
        if (!file.exists())
            throw new FileNotFoundException();
        String str = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        sb.append(" ");
        return sb;
    }

    private static String translateOneWord(String word, HashMap map) {
        if (map.containsKey(word.toLowerCase()))
            return (String) map.get(word.toLowerCase());
        else
            return word;
    }

    public static void translate(String path, HashMap map) {
        File file = new File(path);
        String str = null;
        try {
            str = readFromFileToStringBuilder(path).toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String divisionMarks = ".,!&?:; ()@#$%0123456789/*+";
        int point = 0;
        for (int i = 0; i < str.length(); i++) {
            if (divisionMarks.contains(str.substring(i, i + 1))) {
                System.out.print(translateOneWord(str.substring(point, i), map));
                System.out.print(str.charAt(i));
                point = i + 1;
            }
        }
    }
}
