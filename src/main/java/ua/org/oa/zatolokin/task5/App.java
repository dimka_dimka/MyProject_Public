package ua.org.oa.zatolokin.task5;

import java.io.FileNotFoundException;

/**
 * Created by user on 15.06.2016.
 */
public class App {

    static final String pathToEnRuVocabulary = "EN-RU.txt";
    static final String pathToRuEnVocabulary = "RU-EN.txt";

    public static void main(String[] args) {
        try {
            Translator.translate("textFromTranslateRu", Translator.writeToMapFromFile(pathToRuEnVocabulary));
            System.out.println();
            Translator.translate("textFromTranslateEn", Translator.writeToMapFromFile(pathToEnRuVocabulary));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
