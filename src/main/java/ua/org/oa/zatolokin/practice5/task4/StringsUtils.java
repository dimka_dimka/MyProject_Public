package ua.org.oa.zatolokin.practice5.task4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by user on 25.05.2016.
 */
public class StringsUtils {

    public static String readFromFile(String path) {
        StringBuilder sb = new StringBuilder();
        String str = null;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            while ((str = br.readLine()) != null)
                sb.append(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static String replaceSentence(String path) {
        String str = readFromFile(path);
        String[] sentences = str.split("[.]");
        String word = null;
        for (int i = 0; i < sentences.length; i++)
            sentences[i] = sentences[i].trim();
        String[] words = null;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sentences.length; i++) {
            words = sentences[i].split("[ ]");
            word = words[0];
            words[0] = words[words.length - 1];
            words[words.length - 1] = word;
            for (int j = 0; j < words.length; j++) {
                sb.append(words[j]);
                if (j < words.length - 1)
                    sb.append(" ");
            }
            sb.append(". ");
            sentences[i] = sb.toString();
            sb.delete(0, sb.length());
        }
        for (int i = 0; i < sentences.length; i++) {
            sb.append(sentences[i]);
        }
        return sb.toString();
    }
}
