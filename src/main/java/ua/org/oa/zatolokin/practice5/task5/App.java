package ua.org.oa.zatolokin.practice5.task5;

/**
 * Created by user on 25.05.2016.
 */
public class App {
    public static void main(String[] args) {
       // FileUtils.readWriteFileWithBuffer("books.txt", "newBooks.txt");
       // System.out.println(System.nanoTime()); // 1049657830912200
        FileUtils.readWriteFileWithoutBuffer("books.txt", "newBooks2.txt");
        System.out.println(System.nanoTime()); // 1051395523968480
    }
}
