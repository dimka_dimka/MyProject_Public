package ua.org.oa.zatolokin.practice5.task1and2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 19.05.2016.
 */
public class FileWorking {

    public static void writeToFile(String path, String text) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true));) {
            bw.write(text);
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFromFile(String path) {
        String str = null;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
           /* while (br.read()!=-1){
            }*/
            str = br.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static double sortDigit(String path) {
        List<Integer> listInt = new ArrayList<>();
        String[] str = readFromFile(path).split("[ ]");
        int sum = 0;
        for (int i = 0; i < str.length; i++) {
            listInt.add(Integer.parseInt(str[i]));
            sum+=listInt.get(i);
        }
        System.out.println(listInt.toString());
        return sum/listInt.size();
    }
}
