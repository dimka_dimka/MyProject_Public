package ua.org.oa.zatolokin.practice5.task6;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 27.05.2016.
 */
public class Group implements Serializable{

    private static final long serialVersionUID = -8476311718996001445L;
    private ArrayList<Student> group = new ArrayList<>();


    public ArrayList<Student> getGroup() {
        return group;
    }

    public void setGroup(ArrayList<Student> group) {
        this.group = group;
    }

    public Group(ArrayList<Student> group) {
        this.group = group;
    }

    public Group() {
    }

    public void addToGroup(Student st){
        group.add(st);
    }

    @Override
    public String toString() {
        return "Group{" +
                "group=" + group +
                '}';
    }
}
