package ua.org.oa.zatolokin.practice5.task3;

/**
 * Created by user on 19.05.2016.
 */
public class Student {

    private String name;
    private int ball;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBall() {
        return ball;
    }

    public void setBall(int ball) {
        this.ball = ball;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", ball=" + ball +
                '}';
    }

    public Student(String name, int ball) {
        this.name = name;
        this.ball = ball;
    }
}
