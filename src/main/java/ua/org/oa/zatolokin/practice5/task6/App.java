package ua.org.oa.zatolokin.practice5.task6;

import java.io.*;

/**
 * Created by user on 27.05.2016.
 */
public class App {

    public static void main(String[] args) {

        Group myGroup = new Group();
        myGroup.addToGroup(new Student("Vasya", 25));
        myGroup.addToGroup(new Student("Petya", 23));
        myGroup.addToGroup(new Student("Sveta", 22));
        myGroup.addToGroup(new Student("Misha", 24));
        myGroup.addToGroup(new Student("Olya", 19));

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("group.data"));){
            oos.writeObject(myGroup);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Object obj = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("group.data"));){
            obj = ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(obj.toString());
    }
}
