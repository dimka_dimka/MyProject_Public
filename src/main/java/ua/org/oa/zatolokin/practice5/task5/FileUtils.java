package ua.org.oa.zatolokin.practice5.task5;

import java.io.*;

/**
 * Created by user on 25.05.2016.
 */
public class FileUtils {

    public static void readWriteFileWithBuffer(String path1, String path2) {
        String str = null;
        try (BufferedReader br = new BufferedReader(new FileReader(path1));
             BufferedWriter bw = new BufferedWriter((new FileWriter(path2)))) {
            while ((str = br.readLine()) != null) {
                bw.write(str);
                bw.write("\n");
            }
                bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readWriteFileWithoutBuffer(String path1, String path2) {
        StringBuilder sb = new StringBuilder();
        int ch = 0;
        try (FileReader fr = new FileReader(path1);
             FileWriter fw = new FileWriter(path2)) {
            while ((ch = fr.read()) != -1) {
                fw.write((char)ch);
            }
    } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
