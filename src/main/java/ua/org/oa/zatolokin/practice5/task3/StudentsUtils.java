package ua.org.oa.zatolokin.practice5.task3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 19.05.2016.
 */
public class StudentsUtils {

    public static void parseToMap(String path) {
        Map<String, Integer> map = new HashMap<>(); // for store student's names and summary ball
        Map<String, Integer> map2 = new HashMap<>(); // for store stunet's names and amount of astimates
        Integer ball = 0;
        int count = 0;
        String str = null;
        String[] parseStr = null;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            while ((str = br.readLine()) != null) {
                parseStr = str.split("[-=]");
                parseStr[0] = parseStr[0].trim();
                parseStr[1] = parseStr[1].trim();
                if (map.containsKey(parseStr[0])) {
                    ball = map.get(parseStr[0]);
                    count = map2.get(parseStr[0]);
                    map.put(parseStr[0], ball + Integer.parseInt(parseStr[1]));
                    map2.put(parseStr[0], ++count);
                } else {
                    map.put(parseStr[0], Integer.parseInt(parseStr[1]));
                    map2.put(parseStr[0], 1);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(map.toString());
        System.out.println(map2.toString());
        List<Map.Entry<String, Integer>> list = new ArrayList(map.entrySet());
        List<Map.Entry<String, Integer>> list2 = new ArrayList(map2.entrySet());
        for (int i=0;i<map.size();i++){
            if (list.get(i).getValue()/list2.get(i).getValue()>=90)
                System.out.println("Student: "+list.get(i).getKey()+"; Average ball: "+(double)list.get(i).getValue()/list2.get(i).getValue());
        }
    }
}
