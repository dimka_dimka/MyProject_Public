package ua.org.oa.zatolokin.task4_2.part1;

/**
 * Created by user on 10.05.2016.
 */
public class Demo {

    public static void main (String[] args) {
        MyDequelmpl<String> deq = new MyDequelmpl<>();
        deq.addFirst("Vasya");
        deq.addFirst("Petya");
        deq.addLast("Masha");
        deq.addFirst("Dima");
        deq.addLast("Lena");
        deq.addFirst("Semen");
        System.out.println(deq.toString());
        System.out.println(deq.removeFirst().toString());
        System.out.println(deq.toString());
        System.out.println("~~~~~~~~~~~");
        System.out.println(deq.removeLast().toString());
        System.out.println(deq.toString());
        System.out.println("~~~~~~~~~~~");
        System.out.println(deq.removeLast().toString());
        System.out.println(deq.toString());
        System.out.println(deq.removeFirst().toString());
        System.out.println(deq.toString());
        System.out.println(deq.getLast());
        deq.addFirst("Jenya");
        deq.addLast("Oksana");
        deq.addLast("Anjela");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
        for (Object obj:deq.toArray()) {
            System.out.println(obj.toString());
        }
        MyDeque<String> newDeq = new MyDequelmpl<>();
        newDeq.addFirst("Petya");
       newDeq.addLast("Jenya");
        newDeq.addFirst("Oksana");
        System.out.println("~~~~~~~~~~");
        System.out.println(newDeq.toString());
        System.out.println(deq.containsAll(newDeq));
    }
}
