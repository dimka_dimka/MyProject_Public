package ua.org.oa.zatolokin.task4_2.part1;

/**
 * Created by user on 09.05.2016.
 */
public class MyDequelmpl<E> implements MyDeque<E> {

    private int size = 0; // current amount of elements
    private Node elem = new Node(null, null, null); // ancillary object. It contains a link to the first and to the last element

    @Override
    public void addFirst(E e) {
        if (size == 0) {
            elem.prev = new Node(e, null, null);
            elem.next = elem.prev;
        } else {
            elem.prev.prev = new Node(e, elem.prev, null);
            elem.prev = elem.prev.prev;
        }
        size++;
    }

    @Override
    public void addLast(E e) {
        if (size == 0) {
            elem.next = new Node(e, null, null);
            elem.prev = elem.next;
        }
        elem.next.next = new Node(e, null, elem.next);
        elem.next = elem.next.next;
        size++;
    }

    @Override
    public E removeFirst() {
        E removedElem = (E) elem.prev.element;
        elem.prev = elem.prev.next;
        elem.prev.prev = null;
        size--;
        return removedElem;
    }

    @Override
    public E removeLast() {
        E removedElem = (E) elem.next.element;
        elem.next = elem.next.prev;
        elem.next.next = null;
        size--;
        return removedElem;
    }

    @Override
    public E getFirst() {
        if (size > 0)
            return (E) elem.prev.element;
        else return null;
    }

    @Override
    public E getLast() {
        if (size > 0)
            return (E) elem.next.element;
        else return null;
    }

    @Override
    public void clear() {
        elem.prev = null;
        elem.next = null;
        size = 0;
    }

    @Override
    public Object[] toArray() {
        Object[] arrObj = new Object[size];
        Node<E> node = elem.prev;
        for (int i = 0; i < size; i++) {
            arrObj[i] = (E) node.element;
            node = node.next;
        }
        return arrObj;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        Node<E> node;
        MyDeque<? extends E> newDeq = deque;
        int count;
        for (int i = 0; i < deque.size(); i++) {
            count = 0;
            node = elem.prev;
            while (node != null) {
                if (newDeq.getFirst().equals(node.element))
                    count++;
                node = node.next;
            }
            if (count == 0)
                return false;
            newDeq.removeFirst();
        }
        return true;
    }

    private static class Node<E> {

        public E element;
        public Node<E> next;
        public Node<E> prev;

        @Override
        public String toString() {
            return element.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node<?> node = (Node<?>) o;

            return element.equals(node.element);

        }

        @Override
        public int hashCode() {
            return element.hashCode();
        }

        public Node(E element, Node<E> next, Node<E> prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }
    }

    @Override
    public String toString() { // print from first element to last
        StringBuilder st = new StringBuilder();
        Node<E> node = elem.prev;
        while (node != null) {
            st.append(node.toString().concat(", "));
            node = node.next;
        }
        return st.toString();
    }

    public MyDequelmpl() {

    }
}
