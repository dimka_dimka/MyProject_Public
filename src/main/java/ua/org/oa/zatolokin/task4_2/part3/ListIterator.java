package ua.org.oa.zatolokin.task4_2.part3;

import java.util.Iterator;
/**
 * Created by user on 15.05.2016.
 */
public interface ListIterator<E> extends Iterator<E> {

    boolean hasPrevious();

    E previous();

    void set(E e);

    void remove();
}
