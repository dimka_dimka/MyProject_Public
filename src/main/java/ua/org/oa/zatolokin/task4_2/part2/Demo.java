package ua.org.oa.zatolokin.task4_2.part2;

import java.util.Iterator;

/**
 * Created by user on 10.05.2016.
 */
public class Demo {

    public static void main(String[] args) {
        MyDequelmpl<String> deq = new MyDequelmpl<>();
        deq.addFirst("Vasya");
        deq.addFirst("Petya");
        deq.addLast("Masha");
        deq.addFirst("Dima");
        deq.addLast("Lena");
        deq.addFirst("Semen");
        System.out.println(deq.toString());
        System.out.println(deq.iterator().hasNext());
        System.out.println(deq.iterator().next());
        //   System.out.println(deq.iterator().next());
        deq.iterator().remove();
        //  deq.iterator().remove();
        System.out.println(deq.toString());
        System.out.println("~~~~~~~~~~~~`");
        Iterator<String> it = deq.iterator();
        while (it.hasNext())
            System.out.println(it.next());


    }
}
