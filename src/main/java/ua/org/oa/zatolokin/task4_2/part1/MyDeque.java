package ua.org.oa.zatolokin.task4_2.part1;

/**
 * Created by user on 09.05.2016.
 */
public interface MyDeque<E> {

    void addFirst(E e);

    void addLast(E e);

    E removeFirst();

    E removeLast();

    E getFirst();

    E getLast();

    void clear();

    Object[] toArray();

    int size();

    boolean containsAll(MyDeque<? extends E> deque);
}
