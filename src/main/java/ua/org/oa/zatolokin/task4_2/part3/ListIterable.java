package ua.org.oa.zatolokin.task4_2.part3;

/**
 * Created by user on 15.05.2016.
 */
public interface ListIterable<E> {

    ListIterator<E> listIterator();
}
