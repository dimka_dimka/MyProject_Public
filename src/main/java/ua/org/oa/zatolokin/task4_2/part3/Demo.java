package ua.org.oa.zatolokin.task4_2.part3;

/**
 * Created by user on 10.05.2016.
 */
public class Demo {

    public static void main(String[] args) {
        MyDequelmpl<String> deq = new MyDequelmpl<>();
        deq.addFirst("Vasya");
        deq.addFirst("Petya");
        deq.addLast("Masha");
        deq.addFirst("Dima");
        deq.addLast("Lena");
        deq.addFirst("Semen");
        System.out.println(deq.toString());
        ListIterator<String> listIt = deq.listIterator();
        listIt.next();
        listIt.next();
        listIt.set("Enocentiy");
        System.out.println(deq.toString());
        System.out.println("~~~~~~~~~~~~");
        while (listIt.hasNext())
            System.out.println(listIt.next());
        System.out.println("~~~~~~~~~~~~");
        while (listIt.hasPrevious())
            System.out.println(listIt.previous());
    }
}
