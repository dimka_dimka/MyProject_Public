package ua.org.oa.zatolokin.task6_2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by user on 23.05.2016.
 */
public class FileRedactor {

    public static void createFile(String fileName) {
        File file = new File(fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
                System.out.println("File sucesfull created");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else System.out.println("This file already exists");
    }

    public static void deleteFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
            System.out.println("File deleted");
        } else System.out.println("File not found");
    }

    public static void renameFile(String fileName1, String fileName2) {
        File file = new File(fileName1);
        File file2 = new File(fileName2);
        if (file.exists()) {
            file.renameTo(file2);
            System.out.println("File was sucesfull renamed");
        } else System.out.println("File not found");
    }

    public static void createDir(String dirName) {
        File file = new File(dirName);
        file.mkdir();
        System.out.println("Directory was succesfull created");
    }

    public static void showDirectory(String directoryName) {
        File file = new File(directoryName);
        if (file.exists() && file.isDirectory())
            for (String name : file.list())
                System.out.println(name);
        else System.out.println("Directory not found");
    }

    public static void inputFromConsole() {
        int comand = 1;
        String str = null;
        String str2 = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new UncloseSystemIn(System.in)));) {
            System.out.println("0-EXIT, 1-CREATE NEW FILE, 2-DELETE FILE OR DIRECTORY, 3-RENAME FILE OR DIRECTORY, 4-CREATE DIRECTORY, 5-SHOW DIRECTORY'S CONTENT");
            System.out.println("Please make your choise: ");
            while (comand != 0) {
                comand = Integer.parseInt(br.readLine());
                switch (comand) {
                    case 1: {
                        System.out.println("Please input file name: ");
                        str = br.readLine();
                        createFile(str);
                    }
                    break;
                    case 2: {
                        System.out.println("Please input file name: ");
                        str = br.readLine();
                        deleteFile(str);
                    }
                    break;
                    case 3: {
                        System.out.println("Please input file name you want to rename: ");
                        str = br.readLine();
                        System.out.println("Please input new file name: ");
                        str2 = br.readLine();
                        renameFile(str, str2);
                    }
                    break;
                    case 4: {
                        System.out.println("Please input directory name: ");
                        str = br.readLine();
                        createDir(str);
                    }
                    break;
                    case 5: {
                        System.out.println("Please input directory name: ");
                        str = br.readLine();
                        showDirectory(str);
                    }
                    break;
                }
                if (comand != 0)
                    System.out.println("Please make your choise: ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
