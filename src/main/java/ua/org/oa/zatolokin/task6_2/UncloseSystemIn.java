package ua.org.oa.zatolokin.task6_2;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
/**
 * Created by user on 25.05.2016.
 */
public class UncloseSystemIn extends FilterInputStream {

        protected UncloseSystemIn(InputStream in) {
            super(in);
        }

        @Override
        public void close() throws IOException {
            //NOP
        }

}
