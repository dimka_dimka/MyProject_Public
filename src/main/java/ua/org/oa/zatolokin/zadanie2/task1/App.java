package ua.org.oa.zatolokin.zadanie2.task1;

/**
 * Created by user on 10.04.2016.
 */
public class App {

    public static void main (String[] args){
        System.out.println(StringUtils.conversely("what do you do?"));
        System.out.println(StringUtils.isPolindrom("А роза упала на лапу Азора"));
        System.out.println(StringUtils.cutOrAdd("bubububuuuuuuuuuu"));
        System.out.println(StringUtils.wordsSwapString("mama ne mila ramu, a mojet i mila..."));
        System.out.println(StringUtils.wordSwapSentences("mama mila, ramu. Papa smotrel telek. Ili ne smotrel."));
        System.out.println(StringUtils.areContainsOnly("acbbca dfgfjn . efgkn"));
        System.out.println(StringUtils.isDate("24.07.1986"));
        System.out.println(StringUtils.isMail("dima@gmail.com"));
        System.out.println(StringUtils.telToArray("fghhthbbb +(380)632-28-89"));
    }
}
