package ua.org.oa.zatolokin.zadanie2.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 09.04.2016.
 */
public class StringUtils {

    public static String conversely(String st) { // task 1
        StringBuilder stBuild = new StringBuilder();
        for (int i = st.length() - 1; i >= 0; i--) {
            stBuild.append(st.charAt(i));
        }
        return stBuild.toString();
    }

    public static boolean isPolindrom(String st) { // task 2
        StringBuilder stBuild = new StringBuilder(); // creat new StringBuilder to written there our string without separate simbols
        String[] str = st.split("[, .!?;:]+");
        for (String s : str) {
            stBuild.append(s);
        }
        int len = stBuild.length() - 1; // for a simplicity of writing
        StringBuilder stBuild1 = new StringBuilder();
        StringBuilder stBuild2 = new StringBuilder();
        if (stBuild.length() % 2 == 0)
            stBuild1.append(stBuild, 0, len / 2 + 1); // write to StringBuilder1 first half of StringBuilder
        else stBuild1.append(stBuild, 0, len / 2);
        for (int i = len; i > len / 2; i--) {
            stBuild2.append(stBuild.charAt(i)); // write to StringBuilder2 second half of StringBuilder in a reverse
        }
        return stBuild1.toString().equalsIgnoreCase(stBuild2.toString());
    }

    public static String cutOrAdd(String st) { // task 3
        StringBuilder stBuild = new StringBuilder();
        if (st.length() > 10)
            stBuild.append(st, 0, 6);
        else {
            stBuild.append(st);
            while (stBuild.length() < 12)
                stBuild.append('o');
        }
        return stBuild.toString();
    }

    public static String wordsSwapString(String st) { // task 4
        Pattern pattern = null;
        pattern = pattern.compile("\\w+");
        Matcher matcher = pattern.matcher(st);
        List<String> str = new ArrayList<>();
        while (matcher.find()) {
            str.add(matcher.group());
        }
        if (str.size() > 0) {
            String newSt1 = st.replaceFirst("\\w+", str.get(str.size() - 1)); // replace the first word of the last
            String newSt2 = conversely(newSt1); // convert this string
            String newSt3 = newSt2.replaceFirst("\\w+", conversely(str.get(0))); // replace the first word of the last again
            return conversely(newSt3); // convert this string and return
        } else return "You input empty string!!!";
    }

    public static String wordSwapSentences(String st){ // task 5
        StringBuilder stBuild = new StringBuilder();
        String[] str = st.split("[.]"); // devide our string by point
        for (int i=0;i<str.length;i++){
            stBuild.append((wordsSwapString(str[i])).concat("."));
        }
        return stBuild.toString();
    }

    public static boolean areContainsOnly(String st){ // task 7
        Pattern pattern = null;
        pattern = pattern.compile("[^abc]");
        Matcher matcher = pattern.matcher(st);
        return matcher.find();
    }

    public static boolean isDate (String st){ // task 8
        Pattern pattern = null;
        pattern = pattern.compile("\\A\\d\\d[.]\\d\\d[.]\\d{4}\\Z");
        Matcher matcher = pattern.matcher(st);
        return matcher.find();
    }

    public static boolean isMail (String st) { // task 9
        Pattern pattern = null;
        pattern = pattern.compile("\\A\\w+[@]\\w+[.]com\\Z");
        Matcher matcher = pattern.matcher(st);
        return matcher.find();
    }

    public static List<String> telToArray (String st){ // task 10
        Pattern pattern = null;
        pattern = pattern.compile("[+][(]\\d{3}[)]\\d{3}-\\d{2}-\\d{2}");
        Matcher matcher = pattern.matcher(st);
        List<String> str = new ArrayList<>();
        while (matcher.find()) {
            str.add(matcher.group());
        }
        return str;
    }
}
