package ua.org.oa.zatolokin.zadanie2.task1.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 15.04.2016.
 */
public class MarkdownParser {

    private static List<String> phrase = new ArrayList<>(); // array to contain our phrase

    public static List<String> getPhrase() {
        return phrase;
    }

    private static String parseStr(String st, String regex) { // parse one string
        Pattern pattern = null;
        String str = null;
        pattern = pattern.compile(regex);
        Matcher matcher = pattern.matcher(st);
        if (matcher.find()) {
            str = matcher.group();
        }

        return str;
    }

    private static String toHeader(String st) { // task 1
        int i = 0;
        String str = st.trim();
        while (str.charAt(i) == '#') { // count quantity of '#'
            i++;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<h" + i + ">");
        sb.append(str.substring(i));
        sb.append("</h" + i + ">");
        return sb.toString();
    }

    private static String simpliStr(String st) { // task 2
        StringBuilder sb = new StringBuilder();
        sb.append("<p>");
        sb.append(st);
        sb.append("</p>");
        return sb.toString();
    }

    private static String emphasi1(String st) { // task 4
        String regex1 = "(\\*\\*)(.+)(\\*\\*)";
        String str = parseStr(st, regex1);
        if (str != null) {
            Pattern pattern = Pattern.compile(regex1);
            Matcher matcher = pattern.matcher(st);
            return matcher.replaceAll("<strong>$2</strong>");
        }
        return st;
    }

    private static String emphasi2(String st) { // task 4
        String regex2 = "(\\*)(.+)(\\*)";
        String str = parseStr(st, regex2);
        if (str != null) {
            Pattern pattern = Pattern.compile(regex2);
            Matcher matcher = pattern.matcher(st);
            return matcher.replaceAll("<em>$2</em>");
        }
        return st;
    }

    private static String href(String st) { // task 5
        String regex = "(\\[)(.+)(\\])(\\()(https://www[.].+)(\\))";
        String str = parseStr(st, regex);
        if (str != null) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(st);
            return matcher.replaceAll("<a href=\"$5\">$2</a>");
        }
        return st;
    }

    public static void inputPhrase(String st) {
        String[] startPhrase = st.split("[;!?]+"); // devide phrase to sentences
        phrase.add("<html>");
        phrase.add("<body>");
        for (int i = 0; i < startPhrase.length; i++) {
            applyAll(startPhrase[i]);
        }
        phrase.add("</body>");
        phrase.add("</html>");
        for (int i = 0; i < phrase.size(); i++)
            System.out.println(phrase.get(i));
    }

    private static void applyAll(String st) {
        if (st.trim().startsWith("#")) {
            phrase.add(toHeader(st));
            return;
        }
        phrase.add(href(emphasi2(emphasi1(simpliStr(st)))));
    }

    private MarkdownParser(){}
}
