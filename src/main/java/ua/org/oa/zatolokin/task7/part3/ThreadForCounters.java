package ua.org.oa.zatolokin.task7.part3;

/**
 * Created by user on 31.05.2016.
 */
public class ThreadForCounters implements Runnable {

    private Counters counters;

    public ThreadForCounters(Counters counters) {
        this.counters = counters;
    }

    @Override
    public void run() {
        synchronized (ThreadForCounters.class) {
            while (true) {
                System.out.println(counters.getCount1() == counters.getCount2());
                counters.setCount1(counters.getCount1() + 1);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                counters.setCount2(counters.getCount2() + 1);
            }
        }
    }
}
