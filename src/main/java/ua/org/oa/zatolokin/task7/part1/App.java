package ua.org.oa.zatolokin.task7.part1;

import java.util.Date;

/**
 * Created by user on 31.05.2016.
 */
public class App {

    public static void main(String[] args) {

        new Thread(new PrintTime()).start();

        new Thread(() -> {
            while (true) {
                try {
                    Date date = new Date();
                    System.out.println(PrintTime.formatTime.format(date) + " --------> " + Thread.currentThread());
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
