package ua.org.oa.zatolokin.task7.part1;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by user on 31.05.2016.
 */
public class PrintTime implements Runnable {

    public static final SimpleDateFormat formatTime = new SimpleDateFormat("dd MMM yyyy  HH:mm:ss:SSS"); // time format

    @Override
    public void run() {
        new Thread(() -> { // thread for output from the program
            try (InputStreamReader is = new InputStreamReader(System.in)) {
                if (is.read() != -1)
                    System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        while (true) {
            try {
                Date date = new Date();
                System.out.println(formatTime.format(date) + " --------> " + Thread.currentThread());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
