package ua.org.oa.zatolokin.task7.part2;

/**
 * Created by user on 31.05.2016.
 */
public class App {
    public static void main(String[] args) {

        A a1 = new A();
        A a2 = new A();
        Thread t1 = new Thread(new Tester(a1,a2));
        Thread t2 = new Thread(new Tester(a2,a1));
        t1.start();
        t2.start();
    }
}
