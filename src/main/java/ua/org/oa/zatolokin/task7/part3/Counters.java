package ua.org.oa.zatolokin.task7.part3;

/**
 * Created by user on 31.05.2016.
 */
public class Counters {

    private int count1 = 0;
    private int count2 = 0;

    public int getCount1() {
        return count1;
    }

    public void setCount1(int count1) {
        this.count1 = count1;
    }

    public int getCount2() {
        return count2;
    }

    public void setCount2(int count2) {
        this.count2 = count2;
    }
}
