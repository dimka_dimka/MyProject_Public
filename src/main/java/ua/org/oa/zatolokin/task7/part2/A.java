package ua.org.oa.zatolokin.task7.part2;

/**
 * Created by user on 31.05.2016.
 */
public class A {
    private int value = 0;

    synchronized void setValue(int value){
        this.value = value;
    }

    synchronized int getValue(){
        return value;
    }

    public synchronized boolean equals(Object o){
        A a = (A) o;
        try{
            Thread.sleep(1000);
        }catch(InterruptedException ex){
            System.err.println("Interrupted!");
        }
        return value == a.getValue();
    }
}
