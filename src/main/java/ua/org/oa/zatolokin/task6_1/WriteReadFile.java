package ua.org.oa.zatolokin.task6_1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 19.05.2016.
 */
public class WriteReadFile {

    public static void writeToFile (String path, String text){
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(path, true));) {
            bw.write(text);
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Book> ReadFromFileToCollection(String path){
        List<Book> books = new ArrayList<>();
        String str = " ";
        String[] field = null;
        try (BufferedReader br = new BufferedReader(new FileReader(path))){
            while ((str = br.readLine())!=null) {
                if (str!=null){
                    field = str.split("[;]");
                    books.add(new Book(field[0], field[1], Integer.parseInt(field[2])));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static void writeBookToFile(String path, Book book){
        writeToFile(path, "\n");
        writeToFile(path, book.getTitle());
        writeToFile(path, ";");
        writeToFile(path, book.getAuthor());
        writeToFile(path, ";");
        writeToFile(path, String.valueOf(book.getYear()));
    }
}
