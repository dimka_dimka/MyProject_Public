package ua.org.oa.zatolokin.zadanie3;

/**
 * Created by user on 17.04.2016.
 */
public class App {
    public static void main(String[] args){
        GenericStorage<String> gs = new GenericStorage<>();
        gs.add("Petya");
        gs.add("Vasya");
        gs.add("Seryoja");
        gs.add("Andrey");
        System.out.println(gs.toString());
        System.out.println(gs.get(1));
        gs.update(2,"Katya");
        System.out.println(gs.toString());
        gs.delete(3);
        System.out.println(gs.toString());
        gs.delete("Vasya");
        System.out.println(gs.toString());
    }
}
