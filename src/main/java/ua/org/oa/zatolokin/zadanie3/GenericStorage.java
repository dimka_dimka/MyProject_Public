package ua.org.oa.zatolokin.zadanie3;

import java.util.Arrays;

/**
 * Created by user on 17.04.2016.
 */
public class GenericStorage<T> {

    private static final int DEFOULT_LENGHT = 10;
    private int currentIndex = 0;
    private T[] array;

    public void add(T obj) {
        T[] arr;
        if (currentIndex >= array.length) {
            arr = (T[]) new Object[array.length + 1];
        } else {
            arr = array;
        }
        arr[currentIndex] = obj;
        array = arr;
        currentIndex++;
    }

    public T get(int index) {
        if (index >= 0 && index < array.length)
            return array[index];
        else {
            System.out.println("You entered wrong index!");
            return null;
        }
    }

    public T[] getAll() {
        return array;
    }

    public void update(int index, T obj) {
        if (index >= 0 && index <= currentIndex) {
            array[index] = obj;
        } else System.out.println("Operation wasn't complete!");
    }

    public void delete(int index) {
        if (index >= 0 && index <= currentIndex) {
            T[] arr = (T[]) new Object[array.length - 1];
            for (int i = 0; i < index; i++)
                arr[i] = array[i];
            for (int i = index; i < currentIndex; i++)
                arr[i] = array[i + 1];
            array = arr;
            currentIndex--;
        } else System.out.println("Operation wasn't complete!");
    }

    public void delete(T obj) {
        int n = 0;
        for (int i = 0; i <= currentIndex; i++) {
            if (array[i].equals(obj)) {
                delete(i);
                currentIndex--;
                n++;
            }
        }
        if (n == 0) System.out.println("Such object wasn't find!");
    }

    public int getFullSize() {
        return currentIndex;
    }

    @Override
    public String toString() {
        return "GenericStorage{" + Arrays.toString(array) + '}';
    }

    public GenericStorage() {
        array = (T[]) new Object[DEFOULT_LENGHT];
    }

    public GenericStorage(int size) {
        array = (T[]) new Object[size];
    }
}
