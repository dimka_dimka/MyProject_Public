package ua.org.oa.zatolokin.task4_1;

/**
 * Created by user on 04.05.2016.
 */
public class Computer implements Comparable {

    private String name;
    private int price;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public int compareTo(Object o) {
        Computer com = (Computer)o;
        return this.getPrice() - com.getPrice();
    }

    @Override
    public String toString() {
        return "Computer{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    public Computer(String name, int price) {
        this.name = name;
        this.price = price;
    }
}
