package ua.org.oa.zatolokin.task4_1;

/**
 * Created by user on 04.05.2016.
 */
public class Utils {

    public static <T extends Comparable> T max(T[] obj) {
        T m = obj[0];
            for (int i = 1; i < obj.length; i++) {
                if (m.compareTo(obj[i]) < 0) {
                    m = obj[i];
                }
            }
        return m;
    }
}
