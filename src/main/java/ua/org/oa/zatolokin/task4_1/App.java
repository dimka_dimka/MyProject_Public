package ua.org.oa.zatolokin.task4_1;

/**
 * Created by user on 06.05.2016.
 */
public class App {

    public static void main(String[] args) {
        Integer[] arrInt = {1, 3, 2, 5, 4};
        System.out.println(Utils.max(arrInt));
        String[] arrStr = {"Petya", "Vasya", "Vasylisa", "Arkadiy"};
        System.out.println(Utils.max(arrStr));
        Computer[] com = {new Computer("IBM", 5200), new Computer("Pentium", 7800), new Computer("Intel", 2900)};
        System.out.println(Utils.max(com));
        Automobile[] auto = {new Automobile("Audi", 32000), new Automobile("Mersedes", 45000), new Automobile("Lada", 3000)};
        //Utils.max(auto);
    }
}
