package ua.org.oa.zatolokin.practice3.model;

import ua.org.oa.zatolokin.practice3.libImpl.UserImplLibrary;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by user on 21.07.2016.
 */
public class User extends Entity<Integer> {

    private String name;
    private String login;
    private String password;
    private Date birhday;
    private List<Book> books;

    public User() {
        setId();
        this.books = new ArrayList<>();
    }

    public User(String name, String login, String password, Date birhday) {
        setId();
        this.name = name;
        this.login = login;
        this.password = password;
        this.birhday = birhday;
        this.books = new ArrayList<>();
    }

    public User(String name, String login, String password, Date birhday, List<Book> books) {
        setId();
        this.name = name;
        this.login = login;
        this.password = password;
        this.birhday = birhday;
        this.books = books;
    }

    public void setId() {
        Random rand = new Random();
        int id = 0;
        while (true) {
            int count = 0;
            id = rand.nextInt();
            for (User user : UserImplLibrary.getInstance().getAll()) {
                if (user.getId() == id)
                    count++;
            }
            if (count == 0) {
                this.setId(id);
                break;
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirhday() {
        return birhday;
    }

    public void setBirhday(Date birhday) {
        this.birhday = birhday;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void takeBook(Book book, Date rent) {
        books.add(book);
        book.getUsers().add(this);
        Report report = new Report(book, this, rent);
        book.getReport().add(report);
        System.out.println("This book was succesfull token");
    }

    public void returnBook(Book book, Date returns) {
       // books.remove(book);
        for (Report report : book.getReport()) {
            if (report.getUser().getId() == this.getId())
                report.setReturns(returns);
        }
    }

    public void history(){
        System.out.println("History: \n    Name: " + name + ", login: " + login + ", books: ");
        for (Book book : books){
            System.out.println("Title: " + book.getTitle() + ", author: " + book.getAuthor());
            for (Report report : book.getReport()){
                if (report.getUser().getId() == this.getId()){
                    System.out.println("rent: " + report.getRent() + ", return: " + report.getReturns());
                }
            }
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "id= " + getId() + '\'' +
                "name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", birhday=" + birhday +
                '}';
    }

}