package ua.org.oa.zatolokin.practice3.libImpl;

import ua.org.oa.zatolokin.practice3.model.Entity;

import java.util.List;

/**
 * Created by user on 20.07.2016.
 */
public interface Library<K extends Entity<V>, V> {

    void create(K obj);

    void delete(V id);

    K read(V key);

    void update(K obj, V key);

    List<K> getAll();

    K getById(V id);

    K getBy(String fieldName, String value);
}
