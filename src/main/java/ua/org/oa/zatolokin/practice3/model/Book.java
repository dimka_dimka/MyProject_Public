package ua.org.oa.zatolokin.practice3.model;

import ua.org.oa.zatolokin.practice3.libImpl.BookImplLibrary;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by user on 21.07.2016.
 */
public class Book extends Entity<Integer> {

    private String author;
    private String title;
    private int count;
    private List<User> users;
    private List<Report> report;

    public Book() {
       setId();
        users = new ArrayList<>();
        report = new ArrayList<>();
    }

    public Book(String author, String title, int count) {
        setId();
        this.author = author;
        this.title = title;
        this.count = count;
        users = new ArrayList<>();
        report = new ArrayList<>();
    }

    public Book(String author, String title, int count, List<User> users, List<Report> report) {
        setId();
        this.author = author;
        this.title = title;
        this.count = count;
        this.users = users;
        this.report = report;
    }

    public void setId() {
        Random rand = new Random();
        int id = 0;
        while (true) {
            int count = 0;
            id = rand.nextInt();
            for (Book user : BookImplLibrary.getInstance().getAll()){
                if (user.getId() == id)
                    count++;
            }
            if (count == 0) {
                this.setId(id);
                break;
            }
        }
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Report> getReport() {
        return report;
    }

    public void setReport(List<Report> report) {
        this.report = report;
    }

    public void history(){
        System.out.println("History: \n   Title: " + title + ", author: " + author + ", users: ");
        for (User user : users){
            System.out.println("Name: " + user.getName() + ", login: " + user.getLogin());
            for (Report report : this.report){
                if (report.getUser().getId() == user.getId()){
                    System.out.println("rent: " + report.getRent() + ", return: " + report.getReturns());
                }
            }
        }

    }

    @Override
    public String toString() {
        return "Book{" +
                "id= " + getId() + '\'' +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", count=" + count +
                '}';
    }
}
