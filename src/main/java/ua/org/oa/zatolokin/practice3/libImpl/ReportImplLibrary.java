package ua.org.oa.zatolokin.practice3.libImpl;

import ua.org.oa.zatolokin.practice3.model.Report;
import ua.org.oa.zatolokin.practice3.storage.ReportsStorage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 22.07.2016.
 */
public class ReportImplLibrary implements Library<Report, Integer> {

    private static ReportImplLibrary implLibrary;

    private ReportImplLibrary() {}

    public static synchronized ReportImplLibrary getInstance() {
        if (implLibrary == null) {
            implLibrary = new ReportImplLibrary();
        }
        return implLibrary;
    }

    @Override
    public void create(Report obj) {
        for (Report report : ReportsStorage.getInstance().getStore()) {
            if (report.getId() == obj.getId()) {
                System.out.println("This report already exist!");
                return;
            }
        }
        ReportsStorage.getInstance().getStore().add(obj);
        obj.getBook().getReport().add(obj);
        obj.getUser().getBooks().add(obj.getBook());
    }

    @Override
    public void delete(Integer id) {
        int count = 0;
        for (Report report : ReportsStorage.getInstance().getStore()) {
            if (report.getId() == id) {
                ReportsStorage.getInstance().getStore().remove(report);
                count++;
            }
        }
        if (count > 0)
            System.out.println("Object was succesfull deleted");
        else
            System.out.println("Object with this ID not found");
    }

    @Override
    public Report read(Integer key) {
        for (Report report : ReportsStorage.getInstance().getStore()) {
            if (report.getId() == key) {
                return report;
            }
        }
        System.out.println("Object with this ID not found");
        return null;
    }

    @Override
    public void update(Report obj, Integer key) {
        int count = 0;
        for (Report report : ReportsStorage.getInstance().getStore()) {
            if (report.getId() == key) {
                ReportsStorage.getInstance().getStore().remove(report);
                ReportsStorage.getInstance().getStore().add(obj);
                report.getBook().getReport().remove(report);
                obj.getBook().getReport().add(obj);
                report.getUser().getBooks().remove(report.getBook());
                obj.getUser().getBooks().add(obj.getBook());
                count++;
            }
            if (count > 0)
                System.out.println("Object was succesfull replaced");
            else
                System.out.println("Object with this ID not found");
        }

    }

    @Override
    public List<Report> getAll() {
        return ReportsStorage.getInstance().getStore();
    }

    @Override
    public Report getById(Integer id) {
        for (Report report : ReportsStorage.getInstance().getStore()) {
            if (report.getId() == id) {
                return report;
            }
        }
        System.out.println("Object with this ID not found");
        return null;
    }

    @Override
    public Report getBy(String fieldName, String value) {
        return null;
    }

    public List<Report> getByBook(Integer id){
        List<Report> reports = new ArrayList<>();
        boolean areFound = true;
        for (Report report : ReportsStorage.getInstance().getStore()){
            if (report.getBook().getId() == id) {
                reports.add(report);
                areFound = false;
            }
        }
        if (areFound == true) {
            System.out.println("Book with this ID not found");
            return null;
        } else
            return reports;
    }

    public List<Report> getByUser(Integer id){
        List<Report> reports = new ArrayList<>();
        boolean areFound = true;
        for (Report report : ReportsStorage.getInstance().getStore()){
            if (report.getUser().getId() == id) {
                reports.add(report);
                areFound = false;
            }
        }
        if (areFound == true) {
            System.out.println("User with this ID not found");
            return null;
        } else
            return reports;
    }
}
