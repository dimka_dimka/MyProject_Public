package ua.org.oa.zatolokin.practice3.libImpl;

import ua.org.oa.zatolokin.practice3.model.Book;
import ua.org.oa.zatolokin.practice3.storage.BooksStorage;

import java.util.List;

/**
 * Created by user on 22.07.2016.
 */
public class BookImplLibrary implements Library<Book, Integer>{

    private static BookImplLibrary implLibrary;

    private BookImplLibrary() {}

    public static synchronized BookImplLibrary getInstance() {
        if (implLibrary == null) {
            implLibrary = new BookImplLibrary();
        }
        return implLibrary;
    }

    @Override
    public void create(Book obj) {
        for (Book book : BooksStorage.getInstance().getStore()) {
            if (book.getId() == obj.getId()) {
                System.out.println("This book already exist!");
                return;
            }
        }
        BooksStorage.getInstance().getStore().add(obj);
    }

    @Override
    public void delete(Integer id) {
        int count = 0;
        for (Book book : BooksStorage.getInstance().getStore()) {
            if (book.getId() == id) {
                BooksStorage.getInstance().getStore().remove(book);
                count++;
            }
        }
        if (count > 0)
            System.out.println("Object was succesfull deleted");
        else
            System.out.println("Object with this ID not found");
    }

    @Override
    public Book read(Integer key) {
        for (Book book : BooksStorage.getInstance().getStore()) {
            if (book.getId() == key) {
                return book;
            }
        }
        System.out.println("Object with this ID not found");
        return null;
    }

    @Override
    public void update(Book obj, Integer key) {
        int count = 0;
        for (Book book : BooksStorage.getInstance().getStore()) {
            if (book.getId() == key) {
                BooksStorage.getInstance().getStore().remove(book);
                BooksStorage.getInstance().getStore().add(obj);
                count++;
            }
            if (count > 0)
                System.out.println("Object was succesfull replaced");
            else
                System.out.println("Object with this ID not found");
        }
    }

    @Override
    public List<Book> getAll() {
        return BooksStorage.getInstance().getStore();
    }

    @Override
    public Book getById(Integer id) {
        for (Book book : BooksStorage.getInstance().getStore()) {
            if (book.getId() == id) {
                return book;
            }
        }
        System.out.println("Object with this ID not found");
        return null;
    }

    @Override
    public Book getBy(String fieldName, String value) {
        boolean areFound;
        if (fieldName.equals("title")) {
            areFound = true;
            for (Book book : BooksStorage.getInstance().getStore()) {
                if (book.getTitle().equals(value)) {
                    areFound = false;
                    return book;
                }
            }
            if (areFound == true)
                System.out.println("Object with this TITLE not found");
        }
        if (fieldName.equals("author")) {
            areFound = true;
            for (Book book : BooksStorage.getInstance().getStore()) {
                if (book.getAuthor().equals(value)) {
                    areFound = false;
                    return book;
                }
            }
            if (areFound == true)
                System.out.println("Object with this AUTHOR not found");
        }
        System.out.println("This parameter not found!");
        return null;
    }
}
