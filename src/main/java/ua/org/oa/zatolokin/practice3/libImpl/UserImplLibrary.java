package ua.org.oa.zatolokin.practice3.libImpl;

import ua.org.oa.zatolokin.practice3.model.User;
import ua.org.oa.zatolokin.practice3.storage.UsersStorage;

import java.util.List;

/**
 * Created by user on 21.07.2016.
 */
public class UserImplLibrary implements Library<User, Integer> {

    private static UserImplLibrary implLibrary;

    private UserImplLibrary() {}

    public static synchronized UserImplLibrary getInstance() {
        if (implLibrary == null) {
            implLibrary = new UserImplLibrary();
        }
        return implLibrary;
    }

    @Override
    public void create(User obj) {
        for (User user : UsersStorage.getInstance().getStore()) {
            if (user.getId() == obj.getId()) {
                System.out.println("This user already exist!");
                return;
            }
        }
        UsersStorage.getInstance().getStore().add(obj);
    }

    @Override
    public void delete(Integer id) {
        int count = 0;
        for (User user : UsersStorage.getInstance().getStore()) {
            if (user.getId() == id) {
                UsersStorage.getInstance().getStore().remove(user);
                count++;
            }
        }
        if (count > 0)
            System.out.println("Object was succesfull deleted");
        else
            System.out.println("Object with this ID not found");
    }

    @Override
    public User read(Integer key) {
        for (User user : UsersStorage.getInstance().getStore()) {
            if (user.getId() == key) {
                return user;
            }
        }
        System.out.println("Object with this ID not found");
        return null;
    }

    @Override
    public void update(User obj, Integer key) {
        int count = 0;
        for (User user : UsersStorage.getInstance().getStore()) {
            if (user.getId() == key) {
                UsersStorage.getInstance().getStore().remove(user);
                UsersStorage.getInstance().getStore().add(obj);
                count++;
            }
            if (count > 0)
                System.out.println("Object was succesfull replaced");
            else
                System.out.println("Object with this ID not found");
        }
    }

    @Override
    public List<User> getAll() {
        return UsersStorage.getInstance().getStore();
    }

    @Override
    public User getById(Integer id) {
        for (User user : UsersStorage.getInstance().getStore()) {
            if (user.getId() == id) {
                return user;
            }
        }
        System.out.println("Object with this ID not found");
        return null;
    }

    @Override
    public User getBy(String fieldName, String value) {
        boolean areFound;
        if (fieldName.equals("name")) {
            areFound = true;
            for (User user : UsersStorage.getInstance().getStore()) {
                if (user.getName().equals(value)) {
                    areFound = false;
                    return user;
                }
            }
            if (areFound == true)
                System.out.println("Object with this NAME not found");
        }
        if (fieldName.equals("login")) {
            areFound = true;
            for (User user : UsersStorage.getInstance().getStore()) {
                if (user.getLogin().equals(value)) {
                    areFound = false;
                    return user;
                }
            }
            if (areFound == true)
                System.out.println("Object with this login not found");
        }
        System.out.println("This parameter not found!");
        return null;
    }
}
