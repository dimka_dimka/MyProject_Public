package ua.org.oa.zatolokin.practice3.storage;

import ua.org.oa.zatolokin.practice3.model.Report;

/**
 * Created by user on 22.07.2016.
 */
public class ReportsStorage extends GenericStorage<Report, Integer> {

    private static ReportsStorage storage;

    private ReportsStorage() {
        super();
    }

    public static synchronized ReportsStorage getInstance() {
        if (storage == null) {
            storage = new ReportsStorage();
        }
        return storage;
    }
}
