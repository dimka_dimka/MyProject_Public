package ua.org.oa.zatolokin.practice3.storage;

import ua.org.oa.zatolokin.practice3.model.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 20.07.2016.
 */
public abstract class GenericStorage<K extends Entity<V>, V> {

    //private static GenericStorage genericStorage;
    private List<K> store = new ArrayList<>();
   // private List<String> types = new ArrayList<>();

    public GenericStorage() {
    }

  /*  public static synchronized GenericStorage getInstance() {
        if (genericStorage == null) {
            genericStorage = new GenericStorage();
        }
        return genericStorage;
    }*/

    public List<K> getStore() {
        return store;
    }

    public void setStore(List<K> store) {
        this.store = store;
    }

    @Override
    public String toString() {
        return "Storage{" +
                "store=" + store +
                '}';
    }
}
