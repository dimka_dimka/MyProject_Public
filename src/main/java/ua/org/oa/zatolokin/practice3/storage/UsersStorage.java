package ua.org.oa.zatolokin.practice3.storage;

import ua.org.oa.zatolokin.practice3.model.User;

/**
 * Created by user on 21.07.2016.
 */
public class UsersStorage extends GenericStorage<User, Integer> {

    private static UsersStorage storage;

    private UsersStorage() {
        super();
    }

    public static synchronized UsersStorage getInstance() {
        if (storage == null) {
            storage = new UsersStorage();
        }
        return storage;
    }
}
