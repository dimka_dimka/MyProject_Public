package ua.org.oa.zatolokin.practice3.libImpl;

import ua.org.oa.zatolokin.practice3.model.Entity;
import ua.org.oa.zatolokin.practice3.storage.GenericStorage;

import java.util.List;

/**
 * Created by user on 20.07.2016.
 */
public abstract class CrudLibrary<T extends Entity<Integer>> implements Library<T, Integer> {

    private Class<T> type;
    private GenericStorage storage;

    public CrudLibrary(Class<T> type){
            this.type = type;
        //    storage = GenericStorage.getInstance();
    }

    @Override
    public void create(T obj) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public T read(Integer key) {
        return null;
    }

    @Override
    public void update(T obj, Integer key) {

    }

    @Override
    public List<T> getAll() {
        return null;
    }

    @Override
    public T getById(Integer id) {
        return null;
    }

    @Override
    public T getBy(String fieldName, String value) {
        return null;
    }
}
