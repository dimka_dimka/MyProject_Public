package ua.org.oa.zatolokin.practice3;

import ua.org.oa.zatolokin.practice3.model.Book;
import ua.org.oa.zatolokin.practice3.model.User;

import java.util.Date;

/**
 * Created by user on 21.07.2016.
 */
public class App {

    public static void main(String[] args) {
       // List<Book> books = new ArrayList<>();
        User user = new User("Vasya", "vasil", "123", new Date(86, 07, 12));
        System.out.println(user);
        Book boook2 = new Book("Tolstoy", "Voyna i mir", 10);
        LibraryUtils.addBook(boook2);
        LibraryUtils.addUser(user);
        LibraryUtils.takeBook(user, boook2, new Date(89, 12, 07));
        LibraryUtils.returnBook(user, boook2, new Date(92, 07, 8));
        LibraryUtils.addUser(new User("Petya", "pit", "321", new Date(90, 8, 29)));
        System.out.println(user);
        LibraryUtils.bookHistory(boook2);
        System.out.println("");
        LibraryUtils.userHistory(user);
    }
}
