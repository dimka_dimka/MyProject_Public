package ua.org.oa.zatolokin.practice3.model;

import ua.org.oa.zatolokin.practice3.libImpl.ReportImplLibrary;

import java.util.Date;
import java.util.Random;

/**
 * Created by user on 21.07.2016.
 */
public class Report extends Entity<Integer> {

    private Book book;
    private User user;
    private Date rent;
    private Date returns;

    public Report() {
       setId();
    }

    public Report(Book book, User user, Date rent) {
        setId();
        this.book = book;
        this.rent = rent;
        this.user = user;
    }

    public Report(Book book, User user, Date rent, Date returns) {
        setId();
        this.book = book;
        this.user = user;
        this.rent = rent;
        this.returns = returns;
    }

    public void setId() {
        Random rand = new Random();
        int id = 0;
        while (true) {
            int count = 0;
            id = rand.nextInt();
            for (Report user : ReportImplLibrary.getInstance().getAll()){
                if (user.getId() == id)
                    count++;
            }
            if (count == 0) {
                this.setId(id);
                break;
            }
        }
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getRent() {
        return rent;
    }

    public void setRent(Date rent) {
        this.rent = rent;
    }

    public Date getReturns() {
        return returns;
    }

    public void setReturns(Date returns) {
        this.returns = returns;
    }

    @Override
    public String toString() {
        return "Report{" +
                "id= " + getId() +
                ", book=" + book +
                ", user=" + user +
                ", rent=" + rent +
                ", returns=" + returns +
                '}';
    }
}
