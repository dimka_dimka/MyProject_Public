package ua.org.oa.zatolokin.practice3;

import ua.org.oa.zatolokin.practice3.libImpl.BookImplLibrary;
import ua.org.oa.zatolokin.practice3.libImpl.UserImplLibrary;
import ua.org.oa.zatolokin.practice3.model.Book;
import ua.org.oa.zatolokin.practice3.model.User;

import java.util.Date;

/**
 * Created by user on 22.07.2016.
 */
public class LibraryUtils {

    public static void addUser(User user){
        UserImplLibrary.getInstance().create(user);
    }

    public static void deleteUser(Integer id){
        UserImplLibrary.getInstance().delete(id);
    }

    public static void addBook(Book book){
        BookImplLibrary.getInstance().create(book);
    }

    public static void deleteBook(Integer id){
        BookImplLibrary.getInstance().delete(id);
    }

    public static Book findBookByID(Integer id){
        return BookImplLibrary.getInstance().read(id);
    }

    public static Book findByTitle(String title){
        return BookImplLibrary.getInstance().getBy("title", title);
    }

    public static Book findByAutor(String author){
        return BookImplLibrary.getInstance().getBy("author", author);
    }

    public static void takeBook(User user, Book book, Date rent){
        user.takeBook(book, rent);
    }

    public static void returnBook(User user, Book book, Date returns){
        user.returnBook(book, returns);
    }

    public static void userHistory(User user){
        user.history();
    }

    public static void bookHistory(Book book){
        book.history();
    }
}
