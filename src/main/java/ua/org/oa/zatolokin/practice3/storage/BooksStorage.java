package ua.org.oa.zatolokin.practice3.storage;

import ua.org.oa.zatolokin.practice3.model.Book;

/**
 * Created by user on 22.07.2016.
 */
public class BooksStorage extends GenericStorage<Book, Integer> {

    private static BooksStorage storage;

    private BooksStorage() {
        super();
    }

    public static synchronized BooksStorage getInstance() {
        if (storage == null) {
            storage = new BooksStorage();
        }
        return storage;
    }
}
